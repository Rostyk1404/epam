# Epam
#
#
# Task 3
#
#
# def foo():
#    try:
#        return 1
#        with open('full path to file') as file:
#            print(file.read())
#            return
#    finally:
#        return 2
# result = foo()
# print(result)
# 
# So once the try/except block is left using return, which would set the return value to given - finally blocks 
# will always execute, and should be used to free resources etc. While using there another return - overwrites the 
# original one.
# In my particular case, foo() return 2
