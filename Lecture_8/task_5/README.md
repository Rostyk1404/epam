# Epam
#
#
# Task 5
#
# try:
#    if '1' != 1:
#        raise "Error"
#    else:
#        print("Error has not occurred")
# except "Error":
#    print ("Error")
#
# Result: Invalid code.
#
# Explanation:
# 
#   A new exception class must inherit from a BaseException. There is no such inheritance here.
#