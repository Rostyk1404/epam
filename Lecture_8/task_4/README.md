# Epam
#
#
# Task 4
#
# def foo():
#    try:
#        print(1)
#        return
#    finally:
#        print(2)
#    else:
#        print(3)
# foo()
#
# In this case we will get a SyntaxError: invalid syntax. Because we do not 'except block'.
#
# Basically try/except/else/finally works like this:
#
#   Try: This bock will test the excepted error to occur.
#   Except:  Here you can handle the error
#   Else: If there is not any exception then this block will be executed. 
#   Finally: Finally block always executed either exception generating or not   
#
# Syntax:
#   try:
#       Some Code.... 
#
#   except:
#       optional block
#       Handling of exception (if required)
#
#   else:
#       execute if no exception
#
#   finally:
#       Some code .....(always executed)
#
# Explanation:
#   First try clause is executed i.e. the code between try and except clause.
#   If there is no exception, then only try clause will run, except clause is finished.
#   If any exception occurred, the try clause will be skipped and except clause will run.
#   If any exception occurs, but the except clause within the code doesn’t handle it, it is passed on to the outer 
#        try statements. If the exception left unhandled, then the execution stops.
#   A try statement can have more than one except clause
