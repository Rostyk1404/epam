# Epam
#
#
# Task 2
#
# def foo():
#    try:
#        bar(x,4)
#    finally:
#        print('after bar')
#    print('or this after bar')
# foo()
#
# We will get a NameError: name 'bar' is not defined but finally will run ('after bar'). Because finally will 
#    run in any case