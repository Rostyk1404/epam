import re

sentence = """The reasonable man adapts himself to the world: the unreasonable one persists in trying to adapt the 
    world to himself"""
replace = 'unreasonable'
find = 'reasonable'


def swap_words(sentence, replace, find):
    """Function that provides to swap words.
    Args:
      sentence(str): sentence.
      word(str): word that we replace.
      find(str): word that we will split.
    """
    return replace.join(sentence.split(find))


def swap_words_using_regx(sentence):
    """Function that provides to swap words using regx.
    Args:
      sentence(str): sentence.
    """
    return re.sub(r'reasonable', 'unreasonable', sentence)


def run():
    """Script that will run our functions using try/except"""
    try:
        if replace == None and find == None:
            print(swap_words_using_regx(sentence))
        print(swap_words(sentence, replace, find))

    except:
        pass


if __name__ == '__main__':
    run()
