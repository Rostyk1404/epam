numbers = [4, -9, 8, -11, 8]


def count_negative_numbers(numbers: list) -> int:
    """ Function that return the count of negative numbers.
    Args:
      numbers(list): list of numbers.
    """
    return len(list(filter(lambda x: x < 0, numbers)))


def run():
    """Script that will run our function"""
    print(count_negative_numbers(numbers))


if __name__ == '__main__':
    run()
