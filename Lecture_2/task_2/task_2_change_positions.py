from typing import List

players = ["Ashleigh Barty", "Simona Halep", "Naomi Osaka", "Simona Halep", "Karolina Pliskova", "Elina Svitolina"]


def change_position_players(players: list) -> List:
    """Function that provides to swap words.
    Args:
      players(list): list of players.
    """
    players[0], players[-1] = players[-1], players[0]
    return players


def run():
    """Script that will run our function"""
    print(change_position_players(players))


if __name__ == '__main__':
    run()
