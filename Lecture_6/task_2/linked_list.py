class LinkedList:
    """LinkedList"""

    class __Node:
        """docstring for Node"""

        def __init__(self, value):
            self.value = value
            self.__nextNode = None

    def __init__(self):
        """ Method that initiate variables"""
        self.__firstNode = None
        self.__lastNode = None

    def add(self, value):
        """Method add element"""
        if self.__firstNode == None:
            self.__firstNode = self.__Node(value)
            self.__lastNode = self.__firstNode
        newNode = self.__Node(value)
        self.__lastNode.__nextNode = newNode
        self.__lastNode = newNode

    def len(self):
        """Method return len list"""
        len = 0
        if self.__firstNode == None:
            return len
        currentNode = self.__firstNode
        while currentNode != self.__lastNode:
            currentNode = currentNode.__nextNode
            len += 1
        return len

    def get_by_index(self, index: int):
        """Method give as ability to get element by index"""
        if index + 1 > self.len():
            raise IndexError('list index out of range')
        currentNode = self.__firstNode
        for i in range(index + 1):
            currentNode = currentNode.__nextNode
        return currentNode.value

    def delete(self, element):
        """Method give as ability to delete element"""
        if self.__firstNode.value == element:
            self.__firstNode = self.__firstNode.__nextNode
            return
        previousNode = self.__firstNode
        currentNode = self.__firstNode.__nextNode
        for i in range(self.len() - 1):
            if currentNode.value == element:
                previousNode.__nextNode = currentNode.__nextNode
                return
            previousNode = currentNode
            currentNode = currentNode.__nextNode
        raise Exception("There is not \"" + element + "\" element")

    def delete_by_index(self, index: int):
        """Method to delete as ability to delete an element by index"""
        self.delete(self.get_by_index(index))


