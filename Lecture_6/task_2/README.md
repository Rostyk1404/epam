# Epam
#
#
# To create generic type CustomList ~ the list of values, which has length that is extended when new elements are 
#    added to the list.
#
# CustomList is a collection — the list of values of random type, its size changes dynamically and there is a 
#    possibility to index list elements. Indexation in the list starts with 0.
#
# Values of random type can be located in the list, it should be created empty and a set of original values should 
#    be specified. List length changes while adding and removing elements. The elements can be added and removed using 
#       specific methods. List can be checked whether there is a predetermined value in the list.
#
# List indexing allows to perform the following operations based on indexes:
#
#        * To change and read values of existing elements by using indexer
#
#        * To receive index of predetermined value
#
#        * To remove value based on index
#
# The list can be cleared, its length can be identified, its elements can be received in the form of linked list Item 
#   that is available via link to head.
# Collection CustomList can be used in operator foreach and other constructions that are oriented to the presence of 
#    numerator in class.
#
# The task has two levels of complexity: Low and Advanced.
#
#
# Low level tasks require implementation of the following functionality:
#
#       * Creating of empty user list and the one based on elements set (the elements are stored in CustomList in form 
#               of unidirectional linked list
#       
#       * Adding, removing elements
#
#       * Operations with elements by index
#
#       * Clearing the list, receiving its length
#        
#
#  © Receiving link to linked elements list
#
#   Advanced level tasks require implementation of the following functionality:
#       
#       * All completed tasks of Low level
#
#       * Generating exceptions, specified in xml-comments to class methods
#
#       * Receiving from numerator list for operator foreach
#
#

class LinkedList:
    """LinkedList"""

    class __Node:
        """docstring for Node"""

        def __init__(self, value):
            self.value = value
            self.__nextNode = None

    def __init__(self):
        """ Method that initiate variables"""
        self.__firstNode = None
        self.__lastNode = None

    def add(self, value):
        """Method add element"""
        if self.__firstNode == None:
            self.__firstNode = self.__Node(value)
            self.__lastNode = self.__firstNode
        newNode = self.__Node(value)
        self.__lastNode.__nextNode = newNode
        self.__lastNode = newNode

    def len(self):
        """Method return len list"""
        len = 0
        if self.__firstNode == None:
            return len
        currentNode = self.__firstNode
        while currentNode != self.__lastNode:
            currentNode = currentNode.__nextNode
            len += 1
        return len

    def get_by_index(self, index: int):
        """Method give as ability to get element by index"""
        if index + 1 > self.len():
            raise IndexError('list index out of range')
        currentNode = self.__firstNode
        for i in range(index + 1):
            currentNode = currentNode.__nextNode
        return currentNode.value

    def delete(self, element):
        """Method give as ability to delete element"""
        if self.__firstNode.value == element:
            self.__firstNode = self.__firstNode.__nextNode
            return
        previousNode = self.__firstNode
        currentNode = self.__firstNode.__nextNode
        for i in range(self.len() - 1):
            if currentNode.value == element:
                previousNode.__nextNode = currentNode.__nextNode
                return
            previousNode = currentNode
            currentNode = currentNode.__nextNode
        raise Exception("There is not \"" + element + "\" element")

    def delete_by_index(self, index: int):
        """Method to delete as ability to delete an element by index"""
        self.delete(self.get_by_index(index))


