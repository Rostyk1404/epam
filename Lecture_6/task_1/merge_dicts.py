from collections import Counter, defaultdict

dict_1 = {'a': 100, 'b': 200}
dict_2 = {'a': 200, 'c': 300}
dict_3 = {'a': 300, 'd': 100}


def dsum(*args):
    """
        Function, that receives changeable number of dictionaries (keys - letters, values - numbers)
        and combines them into one dictionary.
        :param *args (tuple) : dicts
        :return: sum of dicts values
    """
    ret = defaultdict(int)
    for x in args:
        for k, v in x.items():
            ret[k] += v
    return dict(ret)


def sum_merged_dicts(*args):
    """
        Function, that receives changeable number of dictionaries (keys - letters, values - numbers)
        and combines them into one dictionary.
        :param *args (tuple) : dicts
        :return: sum of dicts values
    """
    counter = Counter()
    for x in args:
        counter.update(x)
    result = dict(counter)
    return result


def run():
    """Script that will run our functions"""
    print(dsum(dict_1, dict_2, dict_3))
    print(sum_merged_dicts(dict_1, dict_2, dict_3))


if __name__ == '__main__':
    run()
