# Epam
#
#
# Implement a function, that receives changeable number of dictionaries (keys - letters, values - numbers) and combines 
#    them into one dictionary.
#
# Dict values should be summarized in case of identical keys
#
#    def combine _dicts(*args):
#
#        dict_1 = {'a': 100, 'b': 200}
#
#        dict_2 = {'a': 200, 'c’: 300}
#
#        dict_3 = {'a': 300, ‘d': 100}
#
# print(combine dicts(dict_1, dict_2)
#
# >>> {'a': 300, 'b’: 200, ‘c': 300}
#
# print(combine dicts(dict_1, dict_2, dict_3)
#
# >>> {'a': 600, 'b‘: 200, ‘c': 300, 'd’: 100}
#
#

from collections import Counter, defaultdict

dict_1 = {'a': 100, 'b': 200}
dict_2 = {'a': 200, 'c': 300}
dict_3 = {'a': 300, 'd': 100}


def dsum(*args):
    ret = defaultdict(int)
    for x in args:
        for k, v in x.items():
            ret[k] += v
    return dict(ret)


def sum_merged_dicts(*args):
    counter = Counter()
    for x in args:
        counter.update(x)
    result = dict(counter)
    return result


def run():
    print(dsum(dict_1, dict_2, dict_3))
    print(sum_merged_dicts(dict_1, dict_2, dict_3))


if __name__ == '__main__':
    run()


