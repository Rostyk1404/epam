import os
from glob import glob

path_to_folder = '/home/ross/epam/Lecture_10/task_1/'
filename = 'unsorted_names.txt'
full_path_to_file = os.path.join(path_to_folder, filename)


def sort_name(full_path_to_file):
    """
    Function that sorts name in ASC and writes to new file
    :full_path_to_file: full path to file
    """
    names = []
    for files in full_path_to_file:
        if files.endswith('.txt'):
            with open(files, "r") as file:
                name = file.readline()
                print(name)
                while name != "":
                    names.append(name)
                    name = file.readline()

                names.sort()
            with open(full_path_to_file, "w") as new_file:
                for name in names:
                    print(name, file=new_file, end='')


def run():
    """
    Script that run function/functions
    """
    print(sort_name(full_path_to_file))


if __name__ == '__main__':
    run()
