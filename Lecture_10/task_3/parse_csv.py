import os
import csv

students = []

path_to_file = '/home/ross/epam/Lecture_10/task_3/'
full_path_to_file = list(map(lambda files: os.path.join(path_to_file, files), os.listdir(path_to_file)))


def get_top_performerts(number_of_top_students=5):
    """
    Function that prints top 5 students via their marks
    :param number_of_top_students:
    :return:
    """
    list_of_top_students = []
    result = sorted(students, key=lambda student: student['average mark'], reverse=True)
    for student in range(0, number_of_top_students):
        list_of_top_students.append(result[student]['student name'])
    return list_of_top_students


def write_in_desc_age_order(filename):
    """
    Function which receives the sorts students via their age in DESC
    and calls function to write csv file
    filename str: filename
    :return: True if csv file sorted False if not
    """
    students_age_desc = sorted(students, key=lambda student: student['age'], reverse=True)
    csv_writer(filename, students_age_desc)
    return 'File sorted'


def csv_reader(full_path_to_file):
    """
    Read a csv file
    :param full_path_to_file: path to file
    :return: True if file read
    """
    for files in full_path_to_file:
        if files.endswith('.csv'):
            with open(files) as file:
                reader = csv.DictReader(file)
                for row in reader:
                    students.append(row)
                return True


def csv_writer(filename, list):
    """
    Write a csv file
    :param filename : file name
    :param list: list of students
    :return:
    """
    with open(filename, 'w', newline='') as file:
        fieldnames = students[0].keys()
        writer = csv.DictWriter(file, fieldnames)
        writer.writeheader()
        for student in list:
            writer.writerow(student)


def run():
    """
    Script that run function/functions
    """

    print(csv_reader(full_path_to_file))
    print(get_top_performerts())
    print(csv_writer("sorted_students.csv", students))
    print(write_in_desc_age_order("sorted_students.csv"))


if __name__ == '__main__':
    run()
