# Epam
#
#
# Task 2
#
#
# - Implement a function which search for most common words in the file.
# - Use `data/lorem_ipsum.txt` file as an _example._
#   ```
#       pythondef most_common_words(filepath, number_of_words=3):
#       passprint(most_common_words('lorem_ipsum.txt'))
#
#   Output:
# 
#       ['donec', 'etiam', 'aliquam']
#
#   ```
# - NOTE: Remember about dots, commas, capital letters etc.
def most_common_words(filepath, number_of_words=3):
    """
    Function that find 3 most common used words in file
    :param filepath:
    :param number_of_words:
    :return:
    """
    words = {}
    with open(filepath, "r", encoding="utf-8") as file:
        for line in file:
            for word in line.split():
                word = word.lower()
                if word.__contains__(".") or word.__contains__(","):
                    word = word[0:-1]
                if words.get(word) is None:
                    words[word] = 1
                else:
                    words[word] += 1
    counter_of_most_common_words = [0 for i in range(number_of_words)]
    common_words = []
    for key in words.keys():
        if words.get(key) > counter_of_most_common_words[-1]:
            counter_of_most_common_words[-1] = words.get(key)
            counter_of_most_common_words.sort(reverse=True)
    for key in words.keys():
        if counter_of_most_common_words.__contains__(words.get(key)):
            common_words.append(key)
            counter_of_most_common_words.remove(words.get(key))
    return common_words


def run():
    """
    Script that run function/functions
    """
    print(most_common_words('/home/ross/epam/Lecture_10/task_2/lorem_ipsum.txt'))


if __name__ == '__main__':
    run()