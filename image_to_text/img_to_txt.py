import os
import pytesseract
from PIL import Image


class ImgToText:
    def __init__(self, path_to_folder):
        self.path_to_folder = \
            list(map(lambda files: os.path.join(path_to_folder, files), os.listdir(path_to_folder)))

    def ocr_core(self, file):
        """
        This function will handle the core OCR processing of images.
        """
        # We'll use Pillow's Image class to open the image and pytesseract to detect the string in the image
        text = pytesseract.image_to_string(Image.open(os.path.join(
            file)))
        return text

    def write_data(self, file_name, data):
        """
        This function will use to save data in file.
        """
        file_name = os.path.join(os.path.dirname(file_name), 'README.md')
        with open(file_name, "a") as opened_file:
            opened_file.write(data)

    def run(self):
        handle_images = list(map(self.ocr_core, (self.path_to_folder)))
        write_data_to_file = list(map(self.write_data, self.path_to_folder, handle_images))


if __name__ == '__main__':
    ob = ImgToText('/home/ross/Pictures/asd')
    ob.run()
