# Epam
#
#
# Create function Transform, replacing the value of each element of an integer array with the sum of this element value 
# and its index, only if the given array is sorted in the given order (the order is set up by enum value SortOrder). 
# To check, if the array is sorted, the function IsSorted from the Task 1 is called. 
#
#
# Example:
#
#       For (5,17, 24, 88, 33, 2} and "ascending" sort order values in the array do not change;
#
#       For {15,10, 3} and "ascending" sort order values in the array do not change;
#
#       For {15,10, 3} and "descending" sort order the values in the array are changing to {15,11, 5}
#
#
from Lecture_5.task_1.sorted import SortOrder, check_is_sorted


def transform(array: list, sortOrder: SortOrder) -> list:
    if check_is_sorted(array, sortOrder):
        for i in range(len(array)):
            array[i] = array[i] + i
    return array


def run():
    print(transform([4, 3, 2, 1, -13], SortOrder.DSC))


if __name__ == '__main__':
    run()
