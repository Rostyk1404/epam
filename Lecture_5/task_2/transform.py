from Lecture_5.task_1.sorted import SortOrder, check_is_sorted


def transform(array: list, sortOrder: SortOrder) -> list:
    """
    Function that replacing the value of each element of an integer array
    :param array: integer array
    :param sortOrder: class SortOrder
    :return: list
    """
    if check_is_sorted(array, sortOrder):
        for i in range(len(array)):
            array[i] = array[i] + i
    return array


def run():
    """Script that will run our function"""
    print(transform([4, 3, 2, 1, -13], SortOrder.DSC))


if __name__ == '__main__':
    run()
