def sum_geo_elem(a1: int, t: float, alim: int) -> int:
    """
    Function that determining the sum of the first elements
    :param a1:
    :param t:
    :param alim:
    :return:
    """
    result = 0
    i = 0
    while a1 * pow(t, i) > alim:
        result += a1 * pow(t, i)
        i += 1
    return result


def run():
    """Script that will run our function"""
    print(sum_geo_elem(100, 0.5, 20))


if __name__ == '__main__':
    run()
