# Epam
#
#
# Create function SumGeometricElements, determining the sum of the first elements of a decreasing geometric progression 
#    of real numbers with a given initial element of a progression aj and a given progression step t, while the last 
#            element must be greater than a given alim. an is calculated by the formula (an+1 = an * t), 0<t<l.
#
# Example :
#
#    For a progression, where ax = 100, and t = 0.5, the sum of the first elements, grater than alim = 20, 
#        equals to 100+50+25 = 175
#
#
def sum_geo_elem(a1: int, t: float, alim: int) -> int:
    result = 0
    i = 0
    while a1 * pow(t, i) > alim:
        result += a1 * pow(t, i)
        i += 1
    return result


def run():
    print(sum_geo_elem(100, 0.5, 20))


if __name__ == '__main__':
    run()

