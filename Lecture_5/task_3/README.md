# Epam
#
#
# Task 3.
#
# Create function MultArithmeticElements, which determines the multiplication of the first n elements of arithmetic 
#    progression of real numbers with a given initial element of progression a1 and progression step t. an is calculated 
#        by the formula (antl = an +1).

# Example:

#         For ax = 5, t = 3, n = 4 multiplication equals to 5*8*11*14 = 6160 


def multi_arithmetic_elements(a1: int, t: int, n: int) -> int:
    result = a1
    for i in range(1, n):
        result = result * (a1 + t * i)
    return result


def run():
    print(f"Multiplication equals to {multi_arithmetic_elements(5,3, 4)}")


if __name__ == '__main__':
    run()
