def mult_arithmetic_elements(a1: int, t: int, n: int) -> int:
    """
    Function which determines the multiplication of the first n elements
    of arithmetic progression
    :param a1:
    :param t:
    :param n:
    :return: int
    """
    result = a1
    for i in range(1, n):
        result = result * (a1 + t * i)
    return result


def run():
    """Script that will run our function"""
    print(f"Multiplication equals to {mult_arithmetic_elements(a1, t, n)}")


if __name__ == '__main__':
    run()
