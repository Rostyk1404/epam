# Epam
#
#
# Create function IsSorted, determining whether a given array of integer values of arbitrary length is sorted in a 
# given order (the order is set up by enum value SortOrder). Array and sort order are passed by parameters. 
# Function does not change the array.
#
#
from enum import Enum


class SortOrder(Enum):
    ASC = 1
    DSC = 2


def check_is_sorted(array: list, sortOrder: SortOrder) -> bool:
    copy_array = list(array).copy()
    if sortOrder == SortOrder.ASC:
        copy_array = sorted(copy_array)
    elif sortOrder == SortOrder.DSC:
        copy_array = sorted(copy_array, reverse=True)
    return array.__eq__(copy_array)


def run():
    print(check_is_sorted([4, 3, 2, 1, -13], SortOrder.DSC))


if __name__ == '__main__':
    run()

