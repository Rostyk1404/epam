from enum import Enum


class SortOrder(Enum):
    """
        class that takes 2 arguments ASC and DCS
    """
    ASC = 1
    DSC = 2


def check_is_sorted(array: list, sortOrder: SortOrder) -> bool:
    """
    Function that determining whether a given array of integer values
    :param array : array of integer
    :param sortOrder: class SortOrder
    :return: bool
    """
    copy_array = list(array).copy()
    if sortOrder == SortOrder.ASC:
        copy_array = sorted(copy_array)
    elif sortOrder == SortOrder.DSC:
        copy_array = sorted(copy_array, reverse=True)
    return array.__eq__(copy_array)


def run():
    """Script that will run our function"""
    print(check_is_sorted([4, 3, 2, 1, -13], SortOrder.DSC))


if __name__ == '__main__':
    run()
