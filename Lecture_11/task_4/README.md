# Epam
#
#
# Task 4
#
#
# Run the module modules/mod_a.py. Check its result. Explain why does this happen. Try to change x to a list [1,2,3]. 
# Explain the result. Try to change import to from x import * where x - module names. Explain the result.
#
#
# Answer: 
# In the first and second cases, the result will be "5", because import from mod_b is after import from mod_c, 
#    therefore, what we changed x in mod_c does not change anything, since in mod_b it is reassigned. 
# If you swap the imports, the result is the list [1, 2, 3].
# In the third case, the same answer will be "5", and here the sequence of imports is not important, 
#    since mod_c .x seisas only exists in mod_b.