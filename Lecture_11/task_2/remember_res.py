
def remember_result(func):
    last_result = dict()
    last_result[1] = None

    def wrapper(*args):
        for k in last_result:
            print(f"Last_result = '{last_result[k]}'")
        last_result.clear()
        last_result[args] = func(*args)
        return last_result[args]

    return wrapper


@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)
    print(f"Current result = '{result}'")
    return result


if __name__ == '__main__':
    sum_list("a", "b")
    sum_list("abc", "cde")
    sum_list(3, 4, 5)
    sum_list(3, 7, 5)
    sum_list(1, 1, 1)
