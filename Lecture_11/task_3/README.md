# Epam
#
#
# Task 3
#
#
# Implement a decorator call_once which runs a function or method once and caches the result. All consecutive calls to 
#    this function should return cached result no matter the arguments.
#
def call_once(func_to_decorate):
    cache = list()

    def wrapper(*args):
        if not cache:
            cache.append(func_to_decorate(*args))
        return cache[0]

    return wrapper


@call_once
def sum_of_numbers(a, b):
    return a + b


def run():
    print(sum_of_numbers(999, 100))
    print(sum_of_numbers(13, 42))
    print(sum_of_numbers(10, 16))


if __name__ == '__main__':
    run()
