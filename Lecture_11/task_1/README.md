# Epam
#
#
# Task 1
#
#
# Look through file modules/legb.py.
#
# 1) Find a way to call inner_function without moving it from inside of enclosed_function.
#
# 2.1) Modify ONE LINE in inner_function to make it print variable 'a' from global scope.
#
# 2.2) Modify ONE LINE in inner_function to make it print variable 'a' form enclosing function.
#

from functools import wraps

a = "I am global variable!"

""" task 1.1 """


def enclosing_function():
    """
    :return: variable status
    """
    a = "I am variable from enclosed function!"

    def inner_function():
        a = "I am local variable!"
        print(a)

    return inner_function  # now we can call this function


""" task 2.1 """


def global_enclosing_function():
    """
    :return: variable status
    """
    a = "I am variable from enclosed function!"

    def inner_function():
        global a
        print(a)

    return inner_function


""" task 2.2 """


def nonlocal_enclosing_function():
    """
    :return: variable status
    """
    a = "I am variable from enclosed function!"

    def inner_function():
        nonlocal a
        print(a)

    return inner_function


if __name__ == '__main__':
    enclosing_function()()
    global_enclosing_function()()
    nonlocal_enclosing_function()()


 

 

 

 
