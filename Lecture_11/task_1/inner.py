a = "I am global variable!"
""" task 1.1 """


def enclosing_function():
    """
    :return: variable status
    """
    a = "I am variable from enclosed function!"

    def inner_function():
        a = "I am local variable!"
        print(a)

    return inner_function  # now we can call this function


""" task 2.1 """


def global_enclosing_function():
    """
    :return: variable status
    """
    a = "I am variable from enclosed function!"

    def inner_function():
        global a
        print(a)

    return inner_function


""" task 2.2 """


def nonlocal_enclosing_function():
    """
    :return: variable status
    """
    a = "I am variable from enclosed function!"

    def inner_function():
        nonlocal a
        print(a)

    return inner_function


if __name__ == '__main__':
    enclosing_function()()
    global_enclosing_function()()
    nonlocal_enclosing_function()()
