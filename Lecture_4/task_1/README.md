# Epam
#
# Transpose a matrix
#

def transpose_a_matrix(matrix, number_of_columns):
    transposed = []
    for i in range(number_of_columns):
        transposed_row = []

        for row in matrix:
            transposed_row.append(row[i])

        transposed.append(transposed_row)
    return transposed


def run():
    """Function that run the script"""
    matrix = [[0, 1, 2],
              [3, 4, 5]]
    print('Matrix' + str(matrix))
    print(transpose_a_matrix(matrix, 3))


if __name__ == '__main__':
    run()


if __name__ == '__main__':
    run()
