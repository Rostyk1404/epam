# Epam
#
# Fibonacci sequences
#

def fibo(n):
    a, b = 0, 1
    for x in range(1, n+1):
        a, b = b, a + b
        yield a


def fib1(n):
    res = []
    a, b = 0, 1
    for x in range(1, n+1):
        a, b = b, a + b
        res.append(a)
    return res


def fibo_rec(n):
    return n if n<=1 else fibo_rec(n-1) + fibo_rec(n-2)


def run():
    """Script that will run our function"""
    print(list(fibo(10)))
    print(fib1(10))
    print(fibo_rec(10))
