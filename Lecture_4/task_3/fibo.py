def fibo(n):
    """
    Function that count Fibonacci sequences
    :param n int : number
    :return: sum of last to element
    """
    a, b = 0, 1
    for x in range(1, n):
        a, b = b, a + b
        yield a


def fib1(n):
    """
    Function that count Fibonacci sequences
    :param n int : number
    :return: sum of last to element
    """
    res = []
    a, b = 0, 1
    for x in range(1, n):
        a, b = b, a + b
        res.append(a)
    return sum(res)


def fibo_rec(n):
    """
    Function that count Fibonacci sequences
    :param n int : number
    :return: last element
    """
    return n if n <= 1 else fibo_rec(n - 1) + fibo_rec(n - 2)


def run():
    """Script that will run our function"""
    print(sum(list((fibo(10)))))
    print(fib1(10))
    print(fibo_rec(10))


if __name__ == '__main__':
    run()
