# Epam
#
# Binary representation
#
# For all positive integers n calculate the result value, which is equal to sum of '1'  in the binary 
#    representation of n
# 
# Note: Do not use any format function

bin_list = []


def decimal_to_binary(num):
    """ Function to convert decimal number to binary using recursion """
    if num == 0:
        return ""
    bin_list.insert(0, num % 2)
    return decimal_to_binary(int(num // 2))


def run():
    """Script that will run our function"""
    print(decimal_to_binary(10))
    print(sum(bin_list))
