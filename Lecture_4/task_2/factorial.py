from math import factorial


def factor(n):
    """
     Function that calculate the factorial for positive numbers
    :param n: number
    """
    return factorial(int(n))


# variant 2, iteration, correct

def facto(n):
    """
    Function that calculate the factorial for positive numbers
    :param n: number
    """
    factor = 1
    for i in range(1, n + 1):
        factor *= i
    return factor


# variant 3, recursion, correct
def factor_rec(n):
    """
    Function that calculate the factorial for positive numbers
    :param n: number
    """
    if n == 0 or n == 1:
        return 1
    else:
        return n * factor_rec(n - 1)


def run():
    """Script that will run our function"""
    print(factor(8))
    print(facto(8))
    print(factor_rec(8))


if __name__ == '__main__':
    run()
