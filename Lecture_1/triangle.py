from math import sqrt


def area_of_triangle(a, b, c):
    """Function that count triangle area.

    Args:
      a(int or float): traiangle side.
      b(int or float): traiangle side.
      c(int or float): traiangle side.
    Returns:
      calculated value with precision=2
    """
    # checks if triangle
    if a < (b + c) and b < (a + c) and c < (a + b):
        # the semiperimeter of triangle = perimeter/2
        P = (a + b + c) / 2
        # area of triangle by Heron's formula:
        return round(sqrt(P * (P - a) * (P - b) * (P - c)), 2)


def run():
    """Script that will run our function"""
    print(area_of_triangle(4.5, 5.9, 9))


if __name__ == '__main__':
    run()
