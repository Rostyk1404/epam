# Epam
#
# Write the program that prints number from 1 to 100. But for multiple of three print 'Fizz' instead
# of the number and for multiples of five print 'Buzz'. For number which are multiples of both three
# and five prints 'FizzBuzz'.
#
def fizz(n1, n2):
    """Function prints number from 1 to 100. But for multiple of three print 'Fizz' instead
       of the number and for multiples of five print 'Buzz'. For number which are multiples
       of both three and five prints 'FizzBuzz'
       Args:
        n1 (int): start.
        n2 (int): end.
    """
    for x in range(n1, n2 + 1):
        if x % 15 == 0:
            print(f'{x} "fizzbuzz"')
        elif x % 5 == 0:
            print(f'{x} "buzz"')
        elif x % 3 == 0:
            print(f'{x} "fizz"')
        else:
            print(x)


def buzz(n1, n2):
    """Function prints number from 1 to 100. But for multiple of three print 'Fizz' instead
       of the number and for multiples of five print 'Buzz'. For number which are multiples
       of both three and five prints 'FizzBuzz'
       Args:
        n1 (int): start.
        n2 (int): end.
    """
    return ['FizzBuzz' if i % 15 == 0 else 'Fizz' if i % 3 == 0 else 'Buzz' 
                if i % 5 == 0 else str(i) for i in range(n1, n2 + 1)]

