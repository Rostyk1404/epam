CARDS = {
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    '10': 0,
    'J': 0,
    'Q': 0,
    'K': 0,
    'A': 1
}


def play_game(first_card: int, second_card: int) -> str:
    """Function that runs the game
       Args:
        first_card (int): first_card.
        second_card (int): second_card.
    """
    if first_card + second_card >= 10:
        return str(first_card + second_card - 10)
    return "Sum: " + str(first_card + second_card)


def cards_value(card_name: str) -> int:
    """Function that checks cards value
        Args:
         card_name (str): card_name.
    """
    cards = CARDS.get(card_name, "Cheater")
    if cards == "Cheater":
        raise SystemExit("Do not cheat!")
    return cards


def run():
    """Script that will run our function"""
    first_card = input('First card:')
    second_card = input('Second card:')
    first_card_value = cards_value(first_card)
    second_card_value = cards_value(second_card)
    print("Result:", play_game(first_card_value, second_card_value))


if __name__ == '__main__':
    run()
