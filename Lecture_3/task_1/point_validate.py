def validate_point(x: float, y: float):
    """Fuction that determinates whether the PointA(x,y) is in shaded area or not"""
    return y >= abs(x) and y <= 1


def run():
    """Script that will run our function"""
    print(validate_point(0.5, 0.7))


if __name__ == '__main__':
    run()
