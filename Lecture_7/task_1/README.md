# Epam
#
#
# Homework
#
# Task 1
#
# Develop Rectangle and ArrayRectangles with a predefined functionality,
#
# On a Low level it is obligatory:
# To develop Rectangle class with following content:
#
#   2 closed float sideA and sideB (sides A and 8 of the rectangle)
#
#   Constructor with two parameters a and b (parameters specify rectangle sides)
#
#   Constructor with a parameter a (parameter specify side A of a rectangle, side B is always equal to 5)
#
#   Method GetSideA, returning value of the side A
#
#   Method GetSideB, returning value of the side B
#
#   Method Area, calculating and returning the area value
#
#   Method Perimeter, calculating and returning the perimeter value
#
#   Method IsSquare, checking whether current rectangle is shape square or not. Returns true if the shape is square and 
#       false in another case.
#
#   Method ReplaceSides, swapping rectangle sides
#
#
#   Homework
#
#   On Advanced level also needed:
#       Complete Level Low Assignment
#
#   Develop class ArrayRectangles, in which declare:
#
#   Private field rectangle_array - array of rectangles
#   Constructor creating an empty array of rectangles with length n
#   Constructor that receives an arbitrary amount of objects of type Rectangle or an array of objects of type Rectangle.
#
#   Method AddRectangle that adds a rectangle of type Rectangle to the array on the nearest free place and returning 
#        true, or returning false, if there is no free space in the array
#
# Method NumberMaxArea, that returns order number (index) of the rectangle with the maximum area value 
#    (numeration starts fram zero)
#
# Method NumberMinPerimeter, that returns order number(index) of the rectangle with the minimum area value 
#    (numeration starts from zero)
#
# Method NumberSquare, that returns the number of squares in the array of rectangles
#
#
class Rectangle:
    """
    Class rectangle contains of two parameters _a_size and _b_size (sides A and B of rectangle) and
        following methods:

    side_a() - returns value of side_a

    side_b() - returns value of side_b

    area() - returns area of the rectangle

    perimeter() - returns perimeter of the rectangle

    is_square() - returns True if this rectangle is a square

    replace_sides() - replaces size of the rectangle
    """

    def __init__(self, a, b=5):
        self.__side_a = float(a)
        self.__side_b = float(b)

    def __repr__(self):
        return f'{self.side_a}, {self.side_b}'

    @property
    def side_a(self):
        return self.__side_a

    @property
    def side_b(self):
        return self.__side_b

    def area(self):
        return self.side_a * self.side_b

    def perimeter(self):
        return (self.side_a + self.side_b) * 2

    def is_square(self):
        return self.side_a == self.side_b

    def replace_sides(self):
        self.__side_a, self.__side_b = self.side_b, self.side_a

# Advance Rectangle
#

from Lecture_7.task_1.rectangle import Rectangle


class ArrayRectangles:

    def __init__(self, *args, n=None):
        self.n = n
        self.__rectangle_array = []
        if args:
            for rectangle in args:
                self.__rectangle_array.append([rectangle.side_a, rectangle.side_b])

    def __repr__(self):
        return f'{self.__rectangle_array}'

    def add_rectangle(self, obj):
        if self.n > len(self.__rectangle_array):
            if isinstance(obj, Rectangle):
                self.__rectangle_array.append([obj.side_a, obj.side_b])
                return True
            else:
                return False
        else:
            return False

    def number_max_area(self):
        area_array = []
        for obj in self.__rectangle_array:
            area_array.append(Rectangle(obj[0], obj[1]).area())
        return area_array.index(max(area_array))

    def number_min_perimeter(self):
        perimeter_array = []
        for obj in self.__rectangle_array:
            perimeter_array.append(Rectangle(obj[0], obj[1]).perimeter())
        return perimeter_array.index(min(perimeter_array))

    def number_square(self):
        squares = []
        for obj in self.__rectangle_array:
            if Rectangle(obj[0], obj[1]).is_square():
                squares.append(obj)
        return len(squares)
