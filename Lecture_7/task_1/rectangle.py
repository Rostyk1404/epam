class Rectangle:
    """
    Class rectangle contains of two parameters _a_size and _b_size (sides A and B of rectangle) and
        following methods:

    side_a() - returns value of side_a

    side_b() - returns value of side_b

    area() - returns area of the rectangle

    perimeter() - returns perimeter of the rectangle

    is_square() - returns True if this rectangle is a square

    replace_sides() - replaces size of the rectangle
    """

    def __init__(self, a, b=5):
        self.__side_a = float(a)
        self.__side_b = float(b)

    def __repr__(self):
        return f'{self.side_a}, {self.side_b}'

    @property
    def side_a(self):
        return self.__side_a

    @property
    def side_b(self):
        return self.__side_b

    def area(self):
        return self.side_a * self.side_b

    def perimeter(self):
        return (self.side_a + self.side_b) * 2

    def is_square(self):
        return self.side_a == self.side_b

    def replace_sides(self):
        self.__side_a, self.__side_b = self.side_b, self.side_a
