from Lecture_7.task_1.rectangle import Rectangle


class ArrayRectangles:

    def __init__(self, *args, n=None):
        self.n = n
        self.__rectangle_array = []
        if args:
            for rectangle in args:
                self.__rectangle_array.append([rectangle.side_a, rectangle.side_b])

    def __repr__(self):
        return f'{self.__rectangle_array}'

    def add_rectangle(self, obj):
        if self.n > len(self.__rectangle_array):
            if isinstance(obj, Rectangle):
                self.__rectangle_array.append([obj.side_a, obj.side_b])
                return True
            else:
                return False
        else:
            return False

    def number_max_area(self):
        area_array = []
        for obj in self.__rectangle_array:
            area_array.append(Rectangle(obj[0], obj[1]).area())
        return area_array.index(max(area_array))

    def number_min_perimeter(self):
        perimeter_array = []
        for obj in self.__rectangle_array:
            perimeter_array.append(Rectangle(obj[0], obj[1]).perimeter())
        return perimeter_array.index(min(perimeter_array))

    def number_square(self):
        squares = []
        for obj in self.__rectangle_array:
            if Rectangle(obj[0], obj[1]).is_square():
                squares.append(obj)
        return len(squares)
