# Epam
#
#
# Task 2
#
# To create classes Employee, SalesPerson, Manager and Company with predefined functionality.
#
# Low level requires:
# To create basic class Employee and deciare following content:
# 
#   Three closed fields — text field name (employee last name), money fields ~ salary and bonus
#   Public property Name for reading employee's last name
#
#   Public property Salary for reading and recording salary field
#   
#   Constructor with parameters string name and money salary (last name and salary are set)
#   Method SetBonus that sets bonuses to salary, amount of which is delegated/conveyed as bonus
#   Method ToPay that returns the value of summarized salary and bonus.
#
#   To create class SalesPerson as class Employee inheritor and declare within it:
#
#   Closed integer field percent (percent of sales targets plan performance/execution)
#
#   Constructor with parameters: name— employee last name, salary, percent ~ percent of plan performance, 
#    first two of which are passed to basic class constructor
#
#   Redefine method of parent class SetBonus in the following way: if the sales person completed the plan more than 
#        100%, so his bonus is doubled (is multiplied by 2), and if more than 200% - bonus is tripled (is multiplied by 3)
#
#
# Homework
#
# To create class Manager as Employee class inheritor, and declare with it:
# Closed integer field quantity (number of clients, who were served by the manager during a month)
#
# Constructor with parameters string name — employee last name, salary and integer clientAmount ~ number of served 
#   clients, first two of which are passed to basic class constructor.
#
# Redefine method of parent class SetBonus in the following way: if the manager served over 100 clients, his bonus is 
#    increased by 500, and if more than 150 clients ~ by 1000,
# 
#
# Advanced level requires:
#
#   To fully complete Low level tasks.
#   Create class Company and declare within it:
#
#   Constructor that receives employee array of Employee type with arbitrary length
#   Method GiveverbodyBonus with money parameter companyBonus that sets the amount of basic bonus for each employee.
#   Method TotalToPay that returns total amount of salary of all employees including awarded bonus
#
#   Method NameMaxSalary that returns employee last name, who received maximum salary including bonus.

class Employee:
    def __init__(self, surname, salary, bonus):
        self.__surname = surname
        self.__salary = salary
        self.__bonus = bonus

    def __repr__(self):
        return f'{self.__class__.__name__}: {self.name}, total salary: ${self.to_pay()}'

    @property
    def name(self):
        return 'mr.' + self.__surname

    def salary(self, value=None):
        if value:
            self.__salary = value
        return "${:,.2f}".format(self.__salary)

    def bonus(self, bonus=None):
        if bonus:
            self.__bonus = bonus
        return "${:,.2f}".format(self.__bonus)

    def set_bonus(self):
        return self.__bonus * self.__salary / 100

    def to_pay(self):
        return self.__salary + self.set_bonus()


class SalesPerson(Employee):
    def __init__(self, surname, salary, bonus, percent):
        super().__init__(surname, salary, bonus)
        self.__percent = percent

    def set_bonus(self):
        if 100 < self.__percent < 200:
            return super().set_bonus() * 2
        elif self.__percent > 200:
            return super().set_bonus() * 3
        return super().set_bonus()


class Manager(Employee):
    def __init__(self, surname, salary, bonus, quantity):
        super().__init__(surname, salary, bonus)
        self.__quantity = quantity

    def set_bonus(self):
        if 100 < self.__quantity < 150:
            return super().set_bonus() + 500
        elif self.__quantity > 150:
            return super().set_bonus() + 1000
        return super().set_bonus()


class Company:
    def __init__(self, *args):
        self.__employee_array = []
        if args:
            for employee in args:
                self.__employee_array.append(employee)

    def __repr__(self):
        array = []
        if self.__employee_array:
            for emp in self.__employee_array:
                array.append(emp.__repr__())
        return f'{array}'

    def give_everybody_bonus(self, company_bonus):
        if self.__employee_array:
            for emp in self.__employee_array:
                emp.bonus(company_bonus)

    def total_to_pay(self):
        total_sum = 0
        if self.__employee_array:
            for emp in self.__employee_array:
                total_sum += emp.to_pay()
        return "${:,.2f}".format(total_sum)

    def name_max_salary(self):
        salary = []
        if self.__employee_array:
            for emp in self.__employee_array:
                salary.append([emp.to_pay(), emp.name])
        return f'The employee with max salary is {sorted(salary)[-1][1]}.' \
               f' His total salary = ' + "${:,.2f}".format(sorted(salary)[-1][0])
