class Employee:
    def __init__(self, surname, salary, bonus):
        self.__surname = surname
        self.__salary = salary
        self.__bonus = bonus

    def __repr__(self):
        return f'{self.__class__.__name__}: {self.name}, total salary: ${self.to_pay()}'

    @property
    def name(self):
        return 'mr.' + self.__surname

    def salary(self, value=None):
        if value:
            self.__salary = value
        return "${:,.2f}".format(self.__salary)

    def bonus(self, bonus=None):
        if bonus:
            self.__bonus = bonus
        return "${:,.2f}".format(self.__bonus)

    def set_bonus(self):
        return self.__bonus * self.__salary / 100

    def to_pay(self):
        return self.__salary + self.set_bonus()


class SalesPerson(Employee):
    def __init__(self, surname, salary, bonus, percent):
        super().__init__(surname, salary, bonus)
        self.__percent = percent

    def set_bonus(self):
        if 100 < self.__percent < 200:
            return super().set_bonus() * 2
        elif self.__percent > 200:
            return super().set_bonus() * 3
        return super().set_bonus()


class Manager(Employee):
    def __init__(self, surname, salary, bonus, quantity):
        super().__init__(surname, salary, bonus)
        self.__quantity = quantity

    def set_bonus(self):
        if 100 < self.__quantity < 150:
            return super().set_bonus() + 500
        elif self.__quantity > 150:
            return super().set_bonus() + 1000
        return super().set_bonus()


class Company:
    def __init__(self, *args):
        self.__employee_array = []
        if args:
            for employee in args:
                self.__employee_array.append(employee)

    def __repr__(self):
        array = []
        if self.__employee_array:
            for emp in self.__employee_array:
                array.append(emp.__repr__())
        return f'{array}'

    def give_everybody_bonus(self, company_bonus):
        if self.__employee_array:
            for emp in self.__employee_array:
                emp.bonus(company_bonus)

    def total_to_pay(self):
        total_sum = 0
        if self.__employee_array:
            for emp in self.__employee_array:
                total_sum += emp.to_pay()
        return "${:,.2f}".format(total_sum)

    def name_max_salary(self):
        salary = []
        if self.__employee_array:
            for emp in self.__employee_array:
                salary.append([emp.to_pay(), emp.name])
        return f'The employee with max salary is {sorted(salary)[-1][1]}.' \
               f' His total salary = ' + "${:,.2f}".format(sorted(salary)[-1][0])
