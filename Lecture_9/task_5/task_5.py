def count_letters(string):
    """
    Function that count symbols in word
    :param string (str) : string of elements
    :return: number of letters
    """
    d = {}
    for x in string:
        d[x] = string.count(x)
    return d


def count_letters2(string):
    """
    Function that count symbols in word
    :param string (str) : string of elements
    :return: number of letters
    """
    return {x: str(string.count(x)) for x in string}


def run():
    """
    Script that run function/functions
    """
    print(count_letters("stringsample"))
    print(count_letters2("stringsample"))


if __name__ == '__main__':
    run()
