# Epam
#
#
# Task 5
#
#
# Implement a function, that takes string as an argument and returns a dictionary, that contains letters of given s
#    string as keys and a number of their occurrence as values.
# 
# print(count_letters("stringsample"))>>> 
#    {'s': 2, 't': 1, 'r': 1, 'i': 1,'n': 1, 'g': 1, 'a': 1, 'm': 1,'p': 1, 'l': 1, 'e': 1}
#
#
def count_letters(string):
    """
    Function that count symbols in word
    :param string (str) : string of elements
    :return: number of letters
    """
    d = {}
    for x in string:
        d[x] = string.count(x)
    return d


def count_letters2(string):
    """
    Function that count symbols in word
    :param string (str) : string of elements
    :return: number of letters
    """
    return {x: str(string.count(x)) for x in string}


def run():
    """
    Script that run function/functions
    """
    print(count_letters("stringsample"))
    print(count_letters2("stringsample"))


if __name__ == '__main__':
    run()
