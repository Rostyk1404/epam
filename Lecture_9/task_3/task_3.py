import re


def get_shortest_word(string: str):
    """
    Function that return shortest word in string using regex
    :param string: String
    :return: shortest word
    """
    return min(re.findall(r"[\S']+", string), key=lambda symbol: len(symbol))


def run():
    """
    Script that run function/functions
    """
    print(get_shortest_word('Any pythonistalike namespaces a lot, a? O'))


if __name__ == '__main__':
    run()

