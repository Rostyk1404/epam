# Epam
#
#
# Task 3
#
#
# Implement a function get_shortest_word(s: str) -> str which returns the shortest word in the given string. The word 
#    can contain any symbols except whitespaces ( , \n, \t and so on). If there are multiple shortest words in the 
#            string with a same length return the word that occurs first. Usage of any split functions is forbidden.
# Example:
#   python>>> get_shortest_word('Python is simple and effective!')'is'>>> get_shortest_word('Any pythonistalike 
#        namespaces a lot, a? O')'a'
#
#
import re


def get_shortest_word(string: str):
    """
    Function that return shortest word in string using regex 
    :param string: String
    :return: shortest word
    """
    return min(re.findall(r"[\S']+", string), key=lambda symbol: len(symbol))


def run():
    """
    Script that run function/functions
    """
    print(get_shortest_word('Any pythonistalike namespaces a lot, a? O'))


if __name__ == '__main__':
    run()

