import re


def is_palindrome(word: str):
    """
    Function that checks if word is palindrome
    :param word str: word
    :return: True if word is palindrome and False if isn't
    """
    delete_special_symbols = ''.join(e for e in word.lower() if e.isalnum())
    if delete_special_symbols == delete_special_symbols[::-1]:
        return True
    return False


def is_palindrome_regex(word: str):
    """
    Function that checks if word is palindrome
    :param word str: word
    :return: True if word is palindrome and False if isn't
    """

    delete_special_symbols = re.sub('[^A-Za-z0-9]+', '', word.lower())
    if delete_special_symbols == delete_special_symbols[::-1]:
        return True
    return False


def run():
    """
    Function that run code
    """
    print(is_palindrome("Do geese see God? "))
    print(is_palindrome_regex("Do geese see God? "))


if __name__ == '__main__':
    run()
