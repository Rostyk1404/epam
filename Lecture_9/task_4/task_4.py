from string import ascii_lowercase


def characters_uses_in_all_strings(*args):
    """
     Fucntion that appear characters in all strings
    :param args: characters
    :return: characters in all strings
    """
    sets = [set(sublist1) for sublist2 in args for sublist1 in sublist2]
    return set.intersection(*sets)


def characters_in_one_strings(*args):
    """
    Function hat appear in at least one string
    :param args: characters
    :return: characters in all strings
    """
    sets = [set(sublist1) for sublist2 in args for sublist1 in sublist2]
    return sorted(set.union(*sets))


def characters_at_least_in_two_strings(*args):
    """
    Function hat appear in at least in two strings
    :param args: characters
    :return: characters in all strings
    """

    all_char_used = set(''.join(''.join(string for string in args).lower()))
    chars = {}
    counter = 0
    for letter in all_char_used:
        for string in args:
            if letter in string:
                counter += 1
            if counter >= 2:
                chars[letter] = counter
        counter = 0
    return chars


def characters_that_not_use_in_strings(*args):
    """
     Function hat appear in at least one string
     :param args: characters
     :return: characters in all strings
     """
    string_words = set(*args)
    return sorted(set(ascii_lowercase).difference(*string_words))


def run():
    """
    Script that run function/functions
    """
    print(characters_uses_in_all_strings(["hello", "world", "python"]))  # True
    print(characters_in_one_strings(["hello", "world", "python"]))
    print(characters_at_least_in_two_strings("hello", "world", "python"))
    print(characters_that_not_use_in_strings(["hello", "world", "python"]))


if __name__ == '__main__':
    run()
