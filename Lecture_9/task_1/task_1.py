def replace_element_generator_expression(sentence: str):
    """
    The Join and replace routine which outpeforms the regex implementation. This
    version uses generator expression
    """
    return '"'.join(e.replace('"', "'") for e in sentence.split("'"))


def replace_element_list_comprehension(sentence: str):
    """
    The Join and replace routine which outpeforms the regex implementation. This
    version uses List Comprehension
    """
    return '"'.join([e.replace('"', "'") for e in sentence.split("'")])


def run():
    """
    Script that run function/functions
    """
    print(replace_element_generator_expression(
        "On election night, I think it's very likely the market declares the winner before the news networks,"
        'Ed Mills, policy analyst at Raymond James, told CNN Business.'))
    print(replace_element_list_comprehension(
        "On election night, I think it's very likely the market declares the winner before the news networks,"
        'Ed Mills, policy analyst at Raymond James, told CNN Business.'))


if __name__ == '__main__':
    run()
